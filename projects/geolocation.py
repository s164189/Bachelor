import googlemaps
from datetime import datetime

gmaps = googlemaps.Client(key="AIzaSyATu6aS_euGaCY86W9V97KbaOIrLlctdgU")

# Geocoding an address
#geocode_result = gmaps.geocode('Tellesvej 2')

# Look up an address with reverse geocoding
#reverse_geocode_result = gmaps.reverse_geocode((40.714224, -73.961452))

# Request directions via public transit
now = datetime.now()
directions_result = gmaps.directions("Tellersvej 2 2900 Hellerup",
                                     "Carlshøjvej 21 2800 kgs. Lyngby",
                                     mode="transit",
                                     departure_time=now)
print(directions_result["arrival_time"])