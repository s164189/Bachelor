from algorithms import heuristic_search, smart_bfs
import datetime
import time

adr_in_inner_city = [   "Østerbrogade 95",
						"Malmø gade 1",
                        "Ole Suhrs Gade 2 1354 København",
                        "Nørre Farimagsgade 64A",
                        "Dahlerupsgade 5",
                        "Gl. Kongevej 10 1610 København",
                        "Kalvebod Brygge 20",
                        "Nikolajgade 25",
                        "Bredgade 43",
                        "Østbanegade 9"]

adr_in_greater_city = [ "Øresundsvej 1",
                        "Tietgensgade 25",
                        "Bakkegårds Alle 11",
                        "Jyllingevej 2",
                        "Vilhelm Bergsøes Alle 2",
                        "Stolpegårdsvej 22",
                        "Lyngbygårdsvej 1",
                        "Kollegiebakken 7",
                        "Skodsborgvej 190",
                        "Duntzfelts Alle 21"]


api_key1 = "AIzaSyCtC6tpcXNthmgQu7tg7jMaTUZwBaV1pf4"
api_key2 = "AIzaSyDccFJNrAr7i63SOfU3p3DFDCoWAZTweZQ"
api_key3 = "AIzaSyCXMvY5kGN61bxQOEesDOlIoFzeJU6xmag"
api_key4 = "AIzaSyB3bcXlIXJjme1jCA573fm_vGe8VfObens"



# Time of the tour de rus
departure_time = datetime.datetime(2019,7,7,9).timestamp()

# Choice of API_key. if key is expired that means the 300$ is spent. use another key or make a new one.
api_key = api_key2

# Choice of Held-Karp (True) or state_space_search (False)
heuristic = True

# Choice of adresses
adr = adr_in_greater_city


for i in range(2,11):

	#Destination for the test results
	f = open("smart_med_fixed_adr", "a")

	if heuristic:
		start = time.time()
		a = heuristic_search(adr[0:i], departure_time, api_key1)
		end = time.time()
	else:
		start = time.time()
		a = smart_bfs(adr[0:i], departure_time, api_key1)
		end = time.time()


	rejsetid = a[0] - departure_time
	rute = a[1]
	kald = a[2]
	f.write("n: {}\t Tid: {}\tTid/K: {}\tKald: {}\tRejsetid: {}\tRute: {}\n".format("{:>2}".format(i), "{0:.6f}".format(end - start), "{0:.6f}".format((end - start)/kald),  "{:>5}".format(kald), "{:>8}".format(rejsetid), rute))
	print("n: {}\t Tid: {}\tTid/K: {}\tKald: {}\tRejsetid: {}\tRute: {}".format("{:>2}".format(i), "{0:.6f}".format(end - start), "{0:.6f}".format((end - start)/kald),  "{:>5}".format(kald), "{:>8}".format(rejsetid), rute))
	f.close()
