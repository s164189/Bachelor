import matplotlib.pyplot as plt
import numpy as np

# data tilstandsøgning for indre københavn
n_i = [2,3,4,5,6,7,8,9,10]

tid_i1 = [0.601896, 2.886021, 11.45256, 25.82077, 61.38313, 157.3483, 369.6633, 838.4448, 1849.835]
kald_i1 = [2, 12, 48, 160, 480, 1344, 3584, 9216, 23040]
tid_kald_i1 = [0.300948, 0.240502, 0.238595, 0.161380, 0.127882, 0.117075, 0.103143, 0.090977, 0.080288]
udviklinger_i1 = [2, 9, 28, 75, 186, 441, 1016, 2295, 5110]
rejsetid_i1 = [2502.0,4744.0,6956.0,9074.0,22269.0,24487.0,27003.0,29338.0,31522.0]

tid_i2 = [1.316543,3.307508,1.415415,3.778619,9.183015,21.89474,55.34395,139.9613,303.5287,695.4862,1496.159]
kald_i2 = [2, 12, 48, 160, 480, 1344, 3584, 9216, 23040]
tid_kald_i2 = [0.707707,0.314885,0.191313,0.136842,0.115300,0.104138,0.084690,0.075465,0.064937]
udviklinger_i2 = [2, 9, 28, 75, 186, 441, 1016, 2295, 5110]
rejsetid_i2 = [2502.0,4744.0,6956.0,9074.0,22269.0,24487.0,27003.0,29338.0,31522.0]

# data tilstandsøgning for storkøbenhavn
n_g = [2,3,4,5,6,7,8,9,10]

tid_g1 = [ 0.669131,3.014743,8.404392,20.26499,51.25249,123.4786,286.6954,636.8158,1376.112]
kald_g1 = [2,12,48,160,480,1344,3584,9216,23040]
tid_kald_g1 = [0.334566,0.251229,0.175092,0.126656,0.106776,0.091874,0.079993,0.069099,0.059727]
udviklinger_g1 = [2,9,28,75,186,441,1016,2295,5110]
rejsetid_g1 = [2677.0,5517.0,2677.0,5517.0,9570.0,13446.0,16795.0,20011.0,22585.0,26056.0,29439.0]


# plot af tidsforbrug af smart
g1_tid = [0.669131  ,3.014743   ,8.404392   ,20.26499   ,51.25249   ,123.4786   ,286.6954   ,636.8158   ,1376.112]
g2_tid = [0.657351  ,2.764026   ,8.353590   ,22.100746  ,53.852920  ,128.249321 ,297.7352   ,658.4553   ,1491.419131]
i1_tid = [0.601896  ,2.886021   ,11.45256   ,25.82077   ,61.38313   ,157.3483   ,369.6633   ,838.4448   ,1849.835]
i2_tid = [1.415415  ,3.778619   ,9.183015   ,21.89474   ,55.34395   ,139.9613   ,303.5287   ,695.4862   ,1496.159]
avg_tid = [sum(tuple)/len(tuple) for tuple in zip(g1_tid, g2_tid, i1_tid, i2_tid)]


plt.plot(n_g, [i/60 for i in avg_tid], 'b-x', label="Gennemsnit")
plt.plot(n_g, [i/60 for i in g1_tid], 'g--x', label="1. Iteration Indre københavn")
plt.plot(n_i, [i/60 for i in i1_tid], 'r--x', label="1. Iteration Storkøbenhavn")
plt.plot(n_g, [i/60 for i in g2_tid], 'c--x', label="2. Iteration Indre københavn")
plt.plot(n_i, [i/60 for i in i2_tid], 'm--x', label="2. Iteration Storkøbenhavn")
plt.legend(loc="upper left")
plt.grid()
plt.xlabel("Antal stops")
plt.ylabel("Tidsforbrug i minutter")
plt.title("Tidsforbrug for tilstandsøgning")
plt.savefig("plots/tidsforbrug_smart")
plt.close()

# plot af tidsforbrug heu
g1_tid = [0.838493,0.803916,1.294930,1.674608,1.764188,2.501541,2.323178,1.681280,2.399130]
g2_tid = [0.748744,0.776369,0.877189,1.080203,1.306246,1.645199,1.380177,1.045226,1.563947]
i1_tid = [1.686176,1.652823,1.784735,1.885922,2.151392,2.255504,2.550963,3.077201,2.669555]
i2_tid = [0.810329,0.849637,1.091221,1.402737,1.316399,1.509149,1.778154,1.964946,1.754124]
avg_tid = [sum(tuple)/len(tuple) for tuple in zip(g1_tid, g2_tid, i1_tid, i2_tid)]

plt.plot(n_g, [i/60 for i in avg_tid], 'b-x', label="Gennemsnit")
plt.plot(n_g, [i/60 for i in g1_tid], 'g--x', label="1. Iteration Indre københavn")
plt.plot(n_i, [i/60 for i in i1_tid], 'r--x', label="1. Iteration Storkøbenhavn")
plt.plot(n_g, [i/60 for i in g2_tid], 'c--x', label="2. Iteration Indre københavn")
plt.plot(n_i, [i/60 for i in i2_tid], 'm--x', label="2. Iteration Storkøbenhavn")
plt.grid()
plt.ylim(0,0.2)
plt.legend(loc="upper left")
plt.xlabel("Antal stops")
plt.ylabel("Tidsforbrug i sekunder")
plt.title("Tidsforbrug for Held-Karp søgning")
plt.savefig("plots/tidsforbrug_heu.png")
plt.close()


# plot tidsforbrug start, stop begge.
g_tid_h = [2677.0,5916.0,10009.0,15409.0,19969.0,25059.0,24766.0,29556.0,34729.0]
g_tid_s = [2677.0,5517.0,9570.0,13446.0,16795.0,20011.0,22585.0,26056.0,29439.0]
i_tid_h = [2502.0,4936.0,7316.0,9808.0,12404.0,15289.0,17236.0,19554.0,22234.0]
i_tid_s = [2502.0,4744.0,6956.0,9074.0,11359.0,13981.0,16279.0,18450.0,20356.0]
plt.plot(n_g, [i/60 for i in g_tid_h], 'b-x', label="Storkøbenhavn med Held-Karp")
plt.plot(n_g, [i/60 for i in g_tid_s], 'g-x', label="Storkøbenhavn med tilstandssøgning")
plt.plot(n_g, [i/60 for i in i_tid_h], 'r-x', label="Indre By med Held-Karp")
plt.plot(n_g, [i/60 for i in i_tid_s], 'c-x', label="Indre By med tilstandssøgning")
plt.grid()
plt.legend(loc="upper left")
plt.xlabel("Antal stops")
plt.ylabel("Tidsforbrug i minutter")
plt.title("Rejsetiden for fundne ruter med stop i Indre By og Storkøbenhavn")
plt.savefig("plots/rejsetid.png")
plt.close()

g_error = [(h/s-1)*100 for (h,s) in zip(g_tid_h,g_tid_s)]
i_error = [(h/s-1)*100 for (h,s) in zip(i_tid_h,i_tid_s)]
plt.plot(n_g, g_error, 'b-x', label="Storkøbenhavn")
plt.plot(n_g, i_error, 'g-x', label="Indre By")
plt.grid()
plt.legend()
plt.xlabel("Antal stop")
plt.ylabel("Forskel i %")
plt.title("Procentforskellen i rejsetiden mellem Held-Karp og tilstandsøgning i Indre By og Storkøbenhavn")
plt.savefig("plots/procent.png")
plt.close()


# tidsforbrug med start/slut/begge
g1_tid = [0.669131  ,3.014743   ,8.404392   ,20.26499   ,51.25249   ,123.4786   ,286.6954   ,636.8158   ,1376.112]
g2_tid = [0.657351  ,2.764026   ,8.353590   ,22.100746  ,53.852920  ,128.249321 ,297.7352   ,658.4553   ,1491.419131]
i1_tid = [0.601896  ,2.886021   ,11.45256   ,25.82077   ,61.38313   ,157.3483   ,369.6633   ,838.4448   ,1849.835]
i2_tid = [1.415415  ,3.778619   ,9.183015   ,21.89474   ,55.34395   ,139.9613   ,303.5287   ,695.4862   ,1496.159]
tid_ingen = [sum(tuple)/len(tuple) for tuple in zip(g1_tid, g2_tid, i1_tid, i2_tid)]
tid_start = [0.489106,1.209206,3.940496,10.027782,26.364054,72.102860,165.273875,418.276365,895.330451]
tid_slut =[0.468702,1.431988,3.788229,9.513510,26.300683,61.984738,155.461637,384.495935,876.031602]
tid_begge = [0.357285,0.589798,1.520213,3.683249,10.539909,26.507023,63.409287,154.932319,368.447775]
plt.plot(n_g, [i/60 for i in tid_ingen], 'b-x', label="Gennemsnit af de 4 kørsler uden stop specificeret")
plt.plot(n_g, [i/60 for i in tid_start], 'g-x', label="Startstop specificeret")
plt.plot(n_g, [i/60 for i in tid_slut], 'r-x', label="Slutstop specificeret")
plt.plot(n_g, [i/60 for i in tid_begge], 'c-x', label="Både start- og slutstop specificeret")
plt.grid()
plt.legend(loc="upper left")
plt.xlabel("Antal stops")
plt.ylabel("Tidsforbrug i minutter")
plt.title("Tidsforbrug for tilstandsøgning med fastsatte stop")
plt.savefig("plots/tid_start_slut.png")
plt.show()
plt.close()

