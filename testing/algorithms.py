import googlemaps
from sortedcontainers import SortedList as sortedlist
from datetime import datetime, timedelta
import itertools

API_KEY = ""
call = 0

def smart_bfs(locations, departure_time, API, start=None, end=None):
	global call
	call = 0
	global API_KEY
	API_KEY = API
	gmaps = googlemaps.Client(API_KEY)

	# Change locations into a set
	locations = set(locations)

	if end != None:
		locations.remove(end)

	# initialize frontier and expanded nodes.
	expanded_nodes = dict()
	frontier = sortedlist()

	if not start == None:
		frontier.add((departure_time, (frozenset([start]), start), [start]))

	else:
		for location in locations:
			# frontier add (cost_of_route, (visited_locations, current_location), route_taken).
			frontier.add((departure_time, (frozenset([location]), location), [location]))

	# Searching the graph
	while True:
		# Choose the cheapest state (n) in the frontier and remove it from the frontier.
		n = frontier.pop(0)

		# If n is a goal state, return solution.
		if n[1][0] == locations:
			if end != None:
				res = gmaps.directions(n[2][-1], end, mode="transit", departure_time=n[0] + 1800)
				try:
					cost = res[0]['legs'][0]['arrival_time']['value']
				except:
					cost = n[0]+1800 + res[0]['legs'][0]['steps'][0]['duration']['value']

				call += 1
				visited_locations = (frozenset(n[1][0].copy().union([end])), end)
				route_taken = n[2] + [end]
				frontier.add((cost, visited_locations, route_taken))
				continue
			else:
				return (n[0], n[2], call)

		if end in n[1][0]:
			return (n[0], n[2], call)

		# We expand n
		expanded_nodes.update({n[1]: (n[0], n[2])})

		# Get outgoing routes (children) of n. i.e. those locations not yet visited in the state.
		outgoing_routes = list(locations.difference(n[1][0]))
		route_costs = get_cost_of_routes(outgoing_routes, n[1][1], n[0])

		for (destination, cost) in route_costs:
			visited_locations = (frozenset(n[1][0].copy().union([destination])), destination)
			route_taken = n[2] + [destination]

			# Get a list of existing states in the frontier and the expanded nodes.
			list_of_visited_locations_in_frontier = [i[1] for i in frontier]
			list_of_visited_locations_in_expanded_nodes = expanded_nodes.keys()

			# If the state has neither been expanded nor is in the frontier add it.
			if visited_locations not in list_of_visited_locations_in_frontier and visited_locations not in list_of_visited_locations_in_expanded_nodes:
				frontier.add((cost, visited_locations, route_taken))

			# Alternatively if state is already in frontier, maybe we found a better route
			elif visited_locations in list_of_visited_locations_in_frontier:

				# Get values of alternative route
				index_of_alternative_route = list_of_visited_locations_in_frontier.index(visited_locations)
				existing_cost = frontier[index_of_alternative_route][0]

				# If new route is better we update it.
				if cost < existing_cost:
					frontier.pop(index_of_alternative_route)
					frontier.add((cost, visited_locations, route_taken))


def get_cost_of_routes(destinations, origin, time):
	"""
	Calculates the time needed to travel from a origin destination to a list of destinations at a specific time.
	"""
	global call
	gmaps = googlemaps.Client(API_KEY)

	time_of_departure = time + 1800


	google_maps_result = gmaps.distance_matrix(origin, destinations, mode='transit', departure_time=time_of_departure)

	call += len(destinations)
	try:
		costs = [result['duration']['value'] + time_of_departure for result in
		         google_maps_result['rows'][0]['elements']]  # List iteration getting the cost of each route.
	except:
		google_maps_result = gmaps.distance_matrix(origin, destinations, mode='transit', departure_time=time_of_departure)
		costs = [result['duration']['value'] + time_of_departure for result in
		         google_maps_result['rows'][0]['elements']]  # Some times gmaps get a bad result. in that case we try again

	return list(zip(destinations, costs))  # Return the travel time paired up with the coresponding destination


def heuristic_search(locations, time_of_departure, API):
	call = 0
	global API_KEY
	API_KEY = API
	n = len(locations)
	gmaps = googlemaps.Client(API_KEY)
	dist_matrix = [[0] * n for _ in range(n)]

	google_maps_result = gmaps.distance_matrix(locations, locations, mode='transit', departure_time=time_of_departure)
	call = len(locations)**2

	for i in range(0,n):
		for j in range(0,n):
			dist_matrix[i][j] = google_maps_result['rows'][i]['elements'][j]['duration']['value']

	C = {}

	# initialiserer originale værdier.
	for k in range(1, n):
		C[(1 << k, k)] = (dist_matrix[0][k], 0)

	# Itererer subsets af stigende størelse og gemmer resultater i klassisk dynamisk stil
	for subset_size in range(2, n):
		for subset in itertools.combinations(range(1, n), subset_size):
			# Set bits for all nodes in this subset
			bits = 0
			for bit in subset:
				bits |= 1 << bit

			# Find the lowest cost to get to this subset
			for k in subset:
				prev = bits & ~(1 << k)

				res = []
				for m in subset:
					if m == 0 or m == k:
						continue
					res.append((C[(prev, m)][0] + dist_matrix[m][k], m))
				C[(bits, k)] = min(res)

	b = (2 ** n - 1) - 1

	# find optimal pris
	res = []
	for k in range(1, n):
		res.append((C[(b, k)][0] + dist_matrix[k][0], k))
	opt, parent = min(res)

	# og så backtracker vi
	path = []
	for i in range(n - 1):
		path.append(parent)
		new_bits = b & ~(1 << parent)
		_, parent = C[(b, parent)]
		b = new_bits

	# tiløj den implicite start node.
	path.append(0)

	gmaps = googlemaps.Client(API_KEY)

	route = [locations[stop] for stop in list(reversed(path))]

	time = time_of_departure+1800

	for i, (ori, des) in enumerate(zip(route, route[1:])):
		directions_result = gmaps.directions(ori, des, mode="transit", departure_time=time, language="da")
		call += 1
		try:
			time = directions_result[0]['legs'][0]['arrival_time']['value'] + 1800
		except:
			time += directions_result[0]['legs'][0]['steps'][0]['duration']['value'] + 1800



	return (time -1800, route, call)