# Tour de Rus Planlæggeren

Resultatet af Sebastian Tejs Knudsens bachelorprojekt om at udvikle en Tour de Rus planlægger. 

## Kom i gang med at bruge værktøjerne

Disse instruktioner skulle gerne få dig igang med at bruge værktøjerne. Bemærk at brugen af dette værktøj forudsætter python 3 og antager at dette er installeret på din maskine. OS X kommer automatisk med python men med Windows kræves en installering. Python 3 til Windows kan findes [her](https://www.python.org/downloads/windows/)  

### Installering af requirements

Værktøjerne kræver en række pakke for at kunne køre. Start med at installere alle pakkerne specificeret i "requirements.txt". Fra dir af denne mappe kør

```
pip3 install -r requirements.txt
```

## Lokal applikation

Den lokale GUI ligger i "local_app/gui.py. Kør den med

```
python3 local_app/gui.py
```

Dette vil åbne applikationen i et seperat vindue. Selve applikationen bruges at vælge/indtaste input. For nemhedens skyld inkluderer jeg her en API nøgle til brug. Misbrug den venligst ikke ellers må jeg lukke min konto.

```
API nøgle: !Api slettet! lav en ny på google dev console
```
Hvis applikationen køres for ture med flere en 6 stop forvent da en smule ventetid. Ellers burde svar komme næsten med det samme.

## Webapplikation

Webapplikationen ligger i "web_app/app.py". Kør den med

```
python3 web_app/app.py
```

Denne vil hoste hjemmesiden lokalt. Åbn da din browser og gå til

```
http://127.0.0.1:5000/
```

Det anbefales at bruge Chrome da hjemmesiden er optimeret hertil, men alle browsere skulle fungere fint.

## Testing

Testsuiten er placeret i "testing/algorithm_tester.py", og alle resulterende testdata er lagt den mappen ved siden af "testing/data".