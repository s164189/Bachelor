#!/usr/bin/env python3

from flask import Flask, render_template, request, send_file
from backend import main
import os




app = Flask(__name__)

@app.route('/')
def index():
	return render_template('layout.html')

@app.route('/', methods=['POST'])
def calculate_route():
	adresses = request.form['adresses']
	time = request.form['time']
	main(time, adresses)
	return send_file("TDR_proposal.txt", as_attachment=True)

if __name__ == "__main__":
	app.run(host='127.0.0.1', port=5000, debug=True)