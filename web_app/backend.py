import googlemaps
from sortedcontainers import SortedList as sortedlist
from datetime import datetime, timedelta
import itertools

#API_KEY = "AIzaSyATu6aS_euGaCY86W9V97KbaOIrLlctdgU"
API_KEY = "AIzaSyCtC6tpcXNthmgQu7tg7jMaTUZwBaV1pf4"
def read_adresses(s):
	"""
	Reads a string of adresses send by the form from the web aplication. Tests if these are valid and returns them as a list.

	:return:    lis(adresses)
	"""

	try:
		addresses = s.split(',')
	except:
		raise ValueError("Attribute error, could not split properly on ',' check input")

	gmaps = googlemaps.Client(API_KEY)

	for address in addresses:
		query = gmaps.geocode(address)
		if not query: # if there is a problem with the query an empty list is returned. Empty list returns false because Python :)
			raise ValueError('"{}" is not a valid address.'.format(address))

	return addresses


def get_cost_of_routes(destinations, origin, time):
	"""
	Calculates the time needed to travel from a origin destination to a list of destinations at a specific time.
	"""
	gmaps = googlemaps.Client(API_KEY)

	time_of_departure = time + 3600

	google_maps_result = gmaps.distance_matrix(origin, destinations, mode='transit', departure_time=time_of_departure)
	costs = [result['duration']['value'] + time_of_departure for result in google_maps_result['rows'][0]['elements']] # List iteration getting the cost of each route.
	return list(zip(destinations, costs)) # Return the travel time paired up with the coresponding destination

def smart_bfs(locations, departure_time):
	#Change locations into a set
	locations = set(locations)

	# initialize frontier and expanded nodes.
	expanded_nodes = dict()
	frontier = sortedlist()

	for location in locations:
		#frontier add (cost_of_route, (visited_locations, current_location), route_taken).
		frontier.add((departure_time, (frozenset([location]), location), [location]))

	# Searching the graph
	while True:
		# Choose the cheapest state (n) in the frontier and remove it from the frontier.
		n = frontier.pop(0)

		# If n is a goal state, return solution.
		if n[1][0] == locations:
			return n

		# We expand n
		expanded_nodes.update({n[1]:(n[0],n[2])})

		# Get outgoing routes (children) of n. i.e. those locations not yet visited in the state.
		outgoing_routes = list(locations.difference(n[1][0]))
		route_costs = get_cost_of_routes(outgoing_routes, n[1][1], n[0])

		for (destination,cost) in route_costs:
			visited_locations = (frozenset(n[1][0].copy().union([destination])), destination)
			route_taken = n[2] + [destination]


			# Get a list of existing states in the frontier and the expanded nodes.
			list_of_visited_locations_in_frontier = [i[1] for i in frontier]
			list_of_visited_locations_in_expanded_nodes = expanded_nodes.keys()

			# If the state has neither been expanded nor is in the frontier add it.
			if visited_locations not in list_of_visited_locations_in_frontier and visited_locations not in list_of_visited_locations_in_expanded_nodes:
				frontier.add((cost, visited_locations, route_taken))

			# Alternatively if state is already in frontier, maybe we found a better route
			elif visited_locations in list_of_visited_locations_in_frontier:

				# Get values of alternative route
				index_of_alternative_route = list_of_visited_locations_in_frontier.index(visited_locations)
				existing_cost = frontier[index_of_alternative_route][0]

				# If new route is better we update it.
				if cost < existing_cost:
					frontier.pop(index_of_alternative_route)
					frontier.add((cost, visited_locations, route_taken))






def heuristic_search(locations, time_of_departure):
	n = len(locations)
	global API_KEY
	gmaps = googlemaps.Client(API_KEY)
	dist_matrix = [[0] * n for _ in range(n)]

	google_maps_result = gmaps.distance_matrix(locations, locations, mode='transit', departure_time=time_of_departure)

	for i in range(n):
		for j in range(i + 1, n):
			dist_matrix[i][j] = google_maps_result['rows'][i]['elements'][j]['duration']['value']

		C = {}

		# initialiserer originale værdier.
		for k in range(1, n):
			C[(1 << k, k)] = (dist_matrix[0][k], 0)

		# Itererer delsæt af stigende størelse og gemmer resultater i klassisk dynamisk stil
		for subset_size in range(2, n):
			for subset in itertools.combinations(range(1, n), subset_size):

				b = 0
				for b in subset:
					b |= 1 << b

				# Find den laveste rejsetid for at komme til dette delsæt.
				for k in subset:
					prev = b & ~(1 << k)

					res = []
					for m in subset:
						if m == 0 or m == k:
							continue
						res.append((C[(prev, m)][0] + dist_matrix[m][k], m))
					C[(b, k)] = min(res)


		b = (2 ** n - 1) - 1

		# find optimal pris
		res = []
		for k in range(1, n):
			res.append((C[(b, k)][0] + dist_matrix[k][0], k))
		opt, parent = min(res)

		# og så backtracker vi
		path = []
		for i in range(n - 1):
			path.append(parent)
			new_bits = b & ~(1 << parent)
			_, parent = C[(b, parent)]
			b = new_bits

		# tiløj den implicite start node.
		path.append(0)

		route = [locations[stop] for stop in list(reversed(path))]

		return route


def txt_writer(path, time):
	f = open("web_app/TDR_proposal.txt",'w+')
	gmaps = googlemaps.Client(API_KEY)

	f.write("TDR Planlæggerne har fundet følgende foreslag til en rute.\n")
	if len(path) > 8: #Hvis algorithem var heuristisk laver vi den disclaimer på det.
		f.write("Bemærk at ruten er fundet heuristisk, dvs at den måske ikke er den allermest optimale.\n")
	f.write("Husk at passe på hindanden og ikke drikke alt for meget\n\n")

	ari = str(datetime.fromtimestamp(time).time())[0:5]
	dep = str(datetime.fromtimestamp(time + 1800).time())[0:5]


	for i, (ori, des) in enumerate(zip(path, path[1:])):
		print("{} : {}".format(ori, des))
		f.write("{0}. stop {1}. Ankomst kl {2}. Afgang {3}\n".format(i+1, ori, ari, dep))
		directions_result = gmaps.directions(ori,des,mode="transit",departure_time=time, language="da")

		for step in directions_result[0]['legs'][0]['steps']:
			f.write('\t' + step['html_instructions'] + '\n')
		try:
			time = directions_result[0]['legs'][0]['arrival_time']['value'] + 3600
		except:
			time += directions_result[0]['legs'][0]['steps'][0]['duration']['value'] + 3600

		ari = str(datetime.fromtimestamp(time).time())[0:5]
		dep = str(datetime.fromtimestamp(time + 1800).time())[0:5]
		f.write('\n\n')

	try:
		time = directions_result[0]['legs'][0]['arrival_time']['value'] + 3600
	except:
		time += directions_result[0]['legs'][0]['steps'][0]['duration']['value'] + 3600

	ari = str(datetime.fromtimestamp(time).time())[0:5]
	f.write("{0}. stop {1}. Ankomst kl {2}. Afgang ??\n".format(len(path), des, ari))
	f.close()




def main(time, adr):

	adr_lis = read_adresses(adr)
	dep_time = datetime.strptime(time, "%Y-%m-%dT%H:%M").timestamp()

	if len(adr_lis) <= 10:
		res = smart_bfs(adr_lis, dep_time)
		route = res[2]
	else:
		res = heuristic_search(adr_lis, dep_time)
		route = res

	#route = ['Philip Schous Vej 1 2000 Frederiksberg', 'Løgstørgade 34 2100 Denmark', 'Tellersvej 2 2900 Hellerup']
	txt_writer(route, dep_time)


if __name__ == '__main__':
	pass

	#adresses = "Philip Schous Vej 1 2000 Frederiksberg, Tellersvej 2 2900 Hellerup, Løgstørgade 34 2100 Denmark, Carlshøjvej 2800 Denmark,Haraldslundvej 38 2800 Kongens Lyngby,Skodsborgvej 190 2850 Nærum,Jagtvej 69 2100 Denmark,Engelsborgvej 87 Kongens Lyngby,Søborg Hovedgade 92 Søborg,Frugtparken 4 2820 Gentofte"
	#adresses = "Philip Schous Vej 1 2000 Frederiksberg, Tellersvej 2 2900 Hellerup, Løgstørgade 34 2100 Denmark"
	#time = datetime.now().timestamp()
	#main("2019-07-07T12:00", adresses)