from random import randint as random_integer
from scipy.spatial import distance
import sys

call = 0
i = 0
def get_int(pers1, pers2):
    global call
    call += 1

    coord1 = name_to_coord[pers1]
    coord2 = name_to_coord[pers2]
    return distance.euclidean(coord1, coord2)


def find_routes_to_person(person, found_routes):
    return [(route + '-' + person, get_int(route.split('-')[-1], person) + time) for (route, time) in found_routes if person not in route.split('-')]


def dynamic_search(frontier, people):

    if (len(frontier[0][0].split('-')) < len(people)):
        new_frontier = []

        for person in people:

            # Af de indtil videre bedste kendte ruter, hvad er alle ruterne til denne person.
            all_routes_to_person = find_routes_to_person(person, frontier)

            # Finder den bedste rute og tilføjer til frontier
            try:
                new_frontier.append(min(all_routes_to_person, key=lambda t: t[1]))
            except ValueError:
                print(frontier)
                print(person)

        if (len(new_frontier[0][0].split('-')) < len(people)):
            return dynamic_search(new_frontier, people)

    else:
        return frontier








names = {  "Ulla",
            "Christoffer",
            "Glayds",
            #"Vannesa",
            #"Elmira",
            #"Dominique",
            #"Janiece",
            #"Bornelia",
            #"Rosina",
            "Anjanette"
           }

name_to_coord = dict()

name_to_coord.update({"Ulla": (1,1)})
name_to_coord.update({"Christoffer": (5,1)})
name_to_coord.update({"Glayds": (2,1)})
name_to_coord.update({"Vannesa": (1,6)})
name_to_coord.update({"Elmira": (7,8)})
name_to_coord.update({"Dominique": (3,2)})
name_to_coord.update({"Janiece": (2,4)})
name_to_coord.update({"Bornelia": (9,3)})
name_to_coord.update({"Rosina": (8,2)})
name_to_coord.update({"Anjanette": (1,5)})

found_routes = []

for name in names:
    found_routes.append((name, 0))

a = dynamic_search(found_routes, names)
#print(a)
#print(min(list(a.values()), key = lambda t: t[1]))
#print(call)
call = 0