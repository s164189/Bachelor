from sortedcontainers import SortedList as sortedlist
import time
import googlemaps
from datetime import datetime
import random

call = 0
#gmaps = googlemaps.Client(key="AIzaSyB3bcXlIXJjme1jCA573fm_vGe8VfObens")

gmaps = googlemaps.Client(key="AIzaSyATu6aS_euGaCY86W9V97KbaOIrLlctdgU")

names = [   "Ulla",
            "Lilly",
            "Tejs",
            "Toke",
            "Jacob",
            "Kabs",
            "Helene",
            "Christoffer"
           ]



name_to_adress = {  "Ulla": "Tellersvej 2 2900 Denmark",
                    "Lilly": "Løgstørgade 34 2100 Denmark",
                    "Tejs": "Carlshøjvej 2800 Denmark",
                    "Toke": "Haraldslundvej 38, 2800 Kongens Lyngby",
                    "Jacob": "Skodsborgvej 190, 2850 Nærum",
                    "Kabs": "Jagtvej 69 2100 Denmark",
                    "Helene": "Engelsborgvej 87 Kongens Lyngby",
                    "Christoffer": "Søborg Hovedgade 92 Søborg"
                  }

name_to_coord = {   "Ulla": (1,1),
                    "Lilly": (5,4),
                    "Tejs": (3,8),
                    "Toke": (5,1),
                    "Jacob": (7,2),
                    "Kabs": (2,7)
                    }

"""
name_to_adress = {  "Ulla": "Hillerød St.",
                    "Lilly": "Lyngby St.",
                    "Tejs": "Hellerup St.",
                    "Toke": "Nørreport St.",
                    "Jacob": "Avedøre St.",
                    "Kabs": "Køge St."
                  }
"""



def get_arrival_time(origin, destination, dep_time):
    global call
    call += 1

    ori_adr = name_to_adress[origin]
    des_adr = name_to_adress[destination]
    datetime_for_dep_time = datetime.fromtimestamp(dep_time)# + 3600)

    route = gmaps.directions(ori_adr, des_adr, mode="transit", departure_time=datetime_for_dep_time)
    try:
        return route[0]['legs'][0]['arrival_time']['value']
    except:
        print("{}: {}: {}".format(origin, destination, dep_time))
        print(route)


def get_cost_of_routes(destinations, origin, time):
    global call
    call += len(destinations)

    ori_adress = [name_to_adress[origin]]

    des_adresses = []
    for des in destinations:
        des_adresses.append(name_to_adress[des])

    time_of_departure = time + 3600
    google_maps_result = gmaps.distance_matrix(ori_adress, des_adresses, mode='transit', departure_time=time_of_departure)
    costs = []
    for route_result in google_maps_result['rows'][0]['elements']:
        costs.append(route_result['duration']['value'] + time_of_departure)

    return list(zip(destinations, costs))

def smart_bfs(locations, departure_time):
    # initialize frontier and expanded nodes.
    expanded_nodes = dict()
    frontier = sortedlist()

    for location in locations:
        # frontier add (cost_of_route, (visited_locations, current_location), route_taken).
        frontier.add((departuretime_for_tdr_as_timestamp, (frozenset([location]), location), [location]))

    # Searching the graph
    while True:

        # Choose the cheapest state (n) in the frontier and remove it from the frontier.
        n = frontier.pop(0)

        # If n is a goal state, return solution.
        if n[1][0] == locations:
            return n

        # We expand n
        expanded_nodes.update({n[1]:(n[0],n[2])})

        # Get outgoing routes (children) of n. i.e. those locations not yet visited in the state.
        outgoing_routes = list(locations.difference(n[1][0]))
        route_costs = get_cost_of_routes(outgoing_routes, n[1][1], n[0])

        for (destination,cost) in route_costs:
            visited_locations = (frozenset(n[1][0].copy().union([destination])), destination)
            route_taken = n[2] + [destination]


            # Get a list of existing states in the frontier and the expanded nodes.
            list_of_visited_locations_in_frontier = [i[1] for i in frontier]
            list_of_visited_locations_in_expanded_nodes = expanded_nodes.keys()

            # If the state has neither been expanded nor is in the frontier add it.
            if visited_locations not in list_of_visited_locations_in_frontier and visited_locations not in list_of_visited_locations_in_expanded_nodes:
                frontier.add((cost, visited_locations, route_taken))

            # Alternatively if state is already in frontier, maybe we found a better route
            elif visited_locations in list_of_visited_locations_in_frontier:

                # Get values of alternative route
                index_of_alternative_route = list_of_visited_locations_in_frontier.index(visited_locations)
                existing_cost = frontier[index_of_alternative_route][0]

                # If new route is better we update it.
                if cost < existing_cost:
                    frontier.pop(index_of_alternative_route)
                    frontier.add((cost, visited_locations, route_taken))

#departuretime_for_tdr = datetime(2019,3,12,9,0,0)
departuretime_for_tdr = datetime(2019,6,10,12,0,0,0)
departuretime_for_tdr_as_timestamp = departuretime_for_tdr.timestamp()
print(departuretime_for_tdr_as_timestamp)




for i in range(3,9):
    #n = set(random.sample(names, i))
    start = time.time()
    #smart_bfs(n, departuretime_for_tdr_as_timestamp)
    a = smart_bfs(set(names[0:i]), departuretime_for_tdr_as_timestamp)
    end = time.time()
    print("Tid: {}\t Antal: {}\t T/K: {}\t Kald {}".format("{0:.6f}".format(end - start), i, "{0:.6f}".format((end - start)/call),  call))
    call = 0
    #print(a)
    #"(1554133366.0, (frozenset({'Tejs', 'Toke', 'Ulla', 'Lilly', 'Jacob'}), 'Lilly'), ['Jacob', 'Tejs', 'Toke', 'Ulla', 'Lilly'])"
