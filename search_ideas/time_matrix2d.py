from sortedcontainers import SortedList as sortedlist
import time
import googlemaps
import datetime

call = 0
gmaps = googlemaps.Client(key="AIzaSyATu6aS_euGaCY86W9V97KbaOIrLlctdgU")


names = [  "Ulla",
            "Lilly",
            "Tejs",
            "Toke",
            "Jacob",
            "Kabs",
           ]



name_to_adress = {  "Ulla": "Tellersvej 2 2900 Denmark",
                    "Lilly": "Løgstørgade 34 2100 Denmark",
                    "Tejs": "Carlshøjvej 2800 Denmark",
                    "Toke": "Haraldslundvej 38, 2800 Kongens Lyngby",
                    "Jacob": "Skodsborgvej 190, 2850 Nærum",
                    "Kabs": "Jagtvej 69 2100 Denmark"
                  }

now = datetime.datetime(2019,4,1,12,0,0,0)

def get_cost_of_route(destination, origin, time):
    global call
    call += 1

    ori_adress = name_to_adress[origin]

    des_adress = name_to_adress[destination]

    google_maps_result = gmaps.directions(ori_adress, des_adress, mode='transit', departure_time=time)

    return google_maps_result[0]['legs'][0]['duration']['value']



def generate_distances(n, names, time):
    dists = [[0] * n for i in range(n)]
    for i in range(n):
        for j in range(n):
            print(get_cost_of_route(names[i],names[j],time), end=', ')
        print("\n")
    return 2

print(generate_distances(5,names,now))