from random import randint as random_integer
from scipy.spatial import distance
from sortedcontainers import *

call = 0
def uniform_search(people):

    frontier = SortedList()

    for person in people:
        frontier.add((0, person))

    def update_frontier(route):
        for next_stop in people.difference(route[1].split('-')):
            current_stop = route[1].split('-')[-1]
            time_to_reach_next_stop = route[0] + get_edge_value(current_stop, next_stop)
            route_with_next_stop = route[1] + '-' + next_stop
            frontier.add((time_to_reach_next_stop, route_with_next_stop))

    done = False

    while(not done):
        update_frontier(frontier.pop(0))
        if (len(frontier[0][1].split('-')) == len(people)):
            print("Done")
            print(frontier[0])
            done = True
            #print()
            #print(frontier)


def get_edge_value(name_of_person1, name_of_person2):
    global call
    call += 1
    if name_of_person1 == "start":
        return 0

    coord1 = name_to_coord[name_of_person1]
    coord2 = name_to_coord[name_of_person2]
    return distance.euclidean(coord1, coord2)

names = {  "Ulla",
            "Christoffer",
            "Glayds",
            "Vannesa",
            "Elmira",
            "Dominique",
            "Janiece",
            "Bornelia",
            "Rosina",
            "Anjanette"
           }

name_to_coord = dict()

name_to_coord.update({"Ulla": (1,1)})
name_to_coord.update({"Christoffer": (5,1)})
name_to_coord.update({"Glayds": (2,1)})
name_to_coord.update({"Vannesa": (1,6)})
name_to_coord.update({"Elmira": (7,8)})
name_to_coord.update({"Dominique": (3,2)})
name_to_coord.update({"Janiece": (2,4)})
name_to_coord.update({"Bornelia": (9,3)})
name_to_coord.update({"Rosina": (8,2)})
name_to_coord.update({"Anjanette": (1,5)})


uniform_search(names)
print(call)
