from random import randint as random_integer
from scipy.spatial import distance
from sortedcontainers import *
best_time = 1000

def get_edge_value(name_of_person1, name_of_person2):

    coord1 = name_to_coord[name_of_person1]
    coord2 = name_to_coord[name_of_person2]
    return distance.euclidean(coord1, coord2)

def depth_first_search(people):

    def search(route):
        global best_time
        frontier_from_current_stop = SortedList()
        for next_stop in people.difference(route[1].split('-')):

            current_stop = route[1].split('-')[-1]
            time_to_reach_next_stop = route[0] + get_edge_value(current_stop, next_stop)
            route_with_next_stop = route[1] + '-' + next_stop
            frontier_from_current_stop.add((time_to_reach_next_stop, route_with_next_stop))

        if len(frontier_from_current_stop[0][1].split('-')) < len(people) and frontier_from_current_stop[0][0] < best_time:
            for stop in frontier_from_current_stop:
                search(stop)
        elif len(frontier_from_current_stop[0][1].split('-')) == len(people) and frontier_from_current_stop[0][0] < best_time:
            best_time = frontier_from_current_stop[0]

    frontier = SortedList()

    for person in people:
        frontier.add((0, person))

    for stop in frontier:
        search(stop)
    print(best_time)


names = {  "Ulla",
            "Christoffer",
            "Glayds",
            "Vannesa",
            "Elmira",
            "Dominique",
            "Janiece",
            "Bornelia",
            "Rosina",
            "Anjanette"
           }

name_to_coord = dict()

name_to_coord.update({"Ulla": (1,1)})
name_to_coord.update({"Christoffer": (5,1)})
name_to_coord.update({"Glayds": (2,1)})
name_to_coord.update({"Vannesa": (1,6)})
name_to_coord.update({"Elmira": (7,8)})
name_to_coord.update({"Dominique": (3,2)})
name_to_coord.update({"Janiece": (2,4)})
name_to_coord.update({"Bornelia": (9,3)})
name_to_coord.update({"Rosina": (8,2)})
name_to_coord.update({"Anjanette": (1,5)})


depth_first_search(names)



