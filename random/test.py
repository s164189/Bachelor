from random.graph import*


alph = ["T", 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N']

for i in range(10):
    nodes = set(alph[0:i])

    begynd = time.time()
    graph = graph_maker(nodes)
    slut = time.time()

    time_graph = '%.2E' % Decimal(slut - begynd)
    no_elements = len(set(graph))
    #len(set(graph.keys()).union(set(graph.values())))

    begynd = time.time()
    dfs(graph, [start], 0)
    slut = time.time()

    time_traversal = '%.2E' % Decimal(slut - begynd)

    print("no of nodes {}\t time for creating graph {}\t time for dfs {}\t size of graph is {}".format(i, time_graph, time_traversal, no_elements))
    res = []
