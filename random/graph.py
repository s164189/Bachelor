from random import randint as random_integer
import time
from decimal import Decimal

start = 'S'
res = []

def next_layer(graph, newest_layer, lis):
    new_layer = set()
    for node in newest_layer:
        outgoing_edges = set()

        for edge in lis.difference(set(node)):
            outgoing_edges.add((node + edge, get_edge_value()))
            new_layer.add(node+edge)

        graph.update({node: outgoing_edges})

    return new_layer


def graph_maker(lis):
    initial_outgoing_edges = set()
    for edge in lis:
        initial_outgoing_edges.add(edge)

    graph = {start: initial_outgoing_edges}
    newest_layer = start

    while len(newest_layer) > 0:
        newest_layer = next_layer(graph, newest_layer, initial_outgoing_edges)

    return graph

def get_edge_value():
    return random_integer(0,10)

def dfs(graph, path, sum):

    if graph[path[-1]] == set():
        res.append((sum, path))

    for edge in graph[path[-1]]:
        dfs(graph, path + [edge[0]], sum + edge[1])


