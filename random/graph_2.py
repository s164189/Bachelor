from random import randint as random_integer
from scipy.spatial import distance
from sortedcontainers import *
import time
import matplotlib.pyplot as plt


best_path = ""
calls = 0
max_value = 10000

def depth_first_search(people):

    def create_outgoing_edges(graph, node, people):
        global max_value
        global best_path
        for person in set(people.keys()).difference(node.split('-')):

            node0 = node + '-' + person
            graph.update({node0: graph[node] + get_edge_value(person, node)})

            if graph[node0] >= max_value:
                continue

            if not (set(people.keys()).difference(node0.split('-'))): # last stop is reached: (empty set is false, why? because python)
                if max_value > graph[node0]:
                    max_value = graph[node0]
                    best_path = node0

            create_outgoing_edges(graph, node0, people)

    graph = {"start": 0}
    create_outgoing_edges(graph, "start", people)
    return graph


def get_edge_value(person, node):
    global calls
    pers1 = node.split('-')[-1]

    if pers1 == "start":
        return 0

    calls += 1
    coord1 = people[person]
    coord2 = people[node.split('-')[-1]]


    return distance.euclidean(coord1, coord2)

def get_people(file_name):
    dic = dict()
    with open(file_name) as f:
        for input in f:
            a = input.split(',')
            dic.update({a[0]: a[1]})
    return dic


#people = get_people("people.txt")
names = [   "Ulla",
            "Christoffer",
            "Glayds",
            "Vannesa",
            "Elmira",
            "Dominique",
            "Janiece",
            "Cornelia",
            "Rosina",
            "Anjanette",
         ]




ti = []
ca = []
for i in range(len(names)):
    people = dict()
    for name in names[0:i+1]:
        people.update({name: (random_integer(0,10),random_integer(0,10))})

    start = time.time()
    depth_first_search(people)
    end = time.time()
    print("Time elapsed {} \t Number of names {}\t Number of call {}\t bestpath {}".format("{0:.4f}".format(end - start), len(people.keys()), calls, best_path))
    ti.append(end - start)
    ca.append(calls)
    best_path = ""
    calls = 0
    max_value = 10000

plt.plot(ti)
plt.show()
plt.plot(ca)
plt.show()


