#!/usr/bin/env python3

from tkinter import *
from datetime import datetime
import webbrowser
import googlemaps
import os
from sortedcontainers import SortedList as sortedlist
from PIL import Image, ImageTk


API_KEY = ""
title_height = 0
entries_height = 2
date_height = 1
adr_height = 4
xp, yp = 10, 10
yblock = 30
xblock = 200
img_padding = 100
adr_list = []
names = []
entry = {}
label = {}

month_dict = dict(zip(["januar", "februar", "marts", "april", "maj", "juni", "juli", "august", "september", "oktober", "november", "december"], [1,2,3,4,5,6,7,8,9,10,11,12]))

def setup_start_window():
	root.geometry("430x400")
	title = Label(root, text="Velkommen til TDR planlæggeren. Indtast her informationer\n" \
	                         "om din TDR og så vil vi finde den optimale tur. Bemærk at\n" \
	                         "Denne app kræver en Google Maps Api kode. De er gratis så\n" \
	                         "hvis du ikke har en så følg linket og få en.            ", justify=LEFT)

	title.place(x=xp, y=yp + img_padding)

	label_link_to_api = Label(root, text="Link", fg="blue", cursor="hand2")
	label_link_to_api.bind("<Button-1>", lambda e: webbrowser.open_new(
		"https://developers.google.com/maps/documentation/javascript/get-api-key#get-the-api-key"))
	label_link_to_api.place(x=280, y=58+ img_padding)

	## Dato ##
	label_date_picker = Label(root, text="Vælg en dato")
	label_date_picker.place(x=2 * xp, y=yp + 4 * yblock + img_padding)

	list_of_legal_years = [i for i in range(datetime.now().year, datetime.now().year + 2)]
	year.set(list_of_legal_years[0])
	drop_down_year = OptionMenu(root, year, *(list_of_legal_years))
	drop_down_year.place(x=2 * xp + xblock, y=yp + 4 * yblock+ img_padding)

	list_of_legal_months = ["januar", "februar", "marts", "april", "maj", "juni", "juli", "august", "september","oktober", "november", "december"]
	month.set(list_of_legal_months[0])
	drop_down_month = OptionMenu(root, month, *(list_of_legal_months))
	drop_down_month.place(x=2 * xp + xblock + 70, y=yp + 4 * yblock+ img_padding, width=75)

	list_of_legal_days = [i for i in range(1, 32)]
	day.set(list_of_legal_days[0])
	drop_down_day = OptionMenu(root, day, *(list_of_legal_days))
	drop_down_day.place(x=2 * xp + xblock + 144, y=yp + 4 * yblock+ img_padding)

	## Tidspunkt ##
	label_date_picker = Label(root, text="Vælg et tidspunkt")
	label_kl = Label(root, text="kl")
	label_colon = Label(root, text=":")

	label_date_picker.place(x=2 * xp, y=yp + 5 * yblock+ img_padding)
	label_kl.place(x=2 * xp + xblock, y=yp + 5 * yblock+ img_padding)
	label_colon.place(x=2 * xp + xblock+70, y=yp + 5 * yblock+ img_padding)

	list_of_legal_hours = ['0' + str(i) if i < 10 else str(i) for i in range(0, 24)]

	hour.set(list_of_legal_hours[8])
	drop_down_hour = OptionMenu(root, hour, *(list_of_legal_hours))
	drop_down_hour.place(x=2 * xp + xblock +20, y=yp + 5 * yblock+ img_padding)

	list_of_legal_minutes = ["00", "15", "30", "45"]
	minute.set(list_of_legal_minutes[0])
	drop_down_minute = OptionMenu(root, minute, *(list_of_legal_minutes))
	drop_down_minute.place(x=2 * xp + xblock + 80, y=yp + 5 * yblock + img_padding)

	## API kode ##
	label_api_code = Label(root, text="Indtast Gmap API kode")
	label_api_code.place(x=2 * xp, y=yp + 6 * yblock + img_padding)

	api_code.configure(fg="grey")
	api_code.insert(0, "SyATu6aS_euTDH76W9VdgU")
	api_code.place(x=2 * xp + xblock, y=yp + 6 * yblock + img_padding)

	label_no_of_entries = Label(root, text="Vælg antal stops")
	label_no_of_entries.place(x=2 * xp, y=yp + 7 * yblock + img_padding)

	list_of_legal_entries = [i for i in range(2, 11)]
	no_of_entries.set(" ")
	drop_down_menu = OptionMenu(root, no_of_entries, *(list_of_legal_entries), command=create_adress_entries)
	drop_down_menu.place(x=2 * xp + xblock, y=yp + 7 * yblock + img_padding)


def create_adress_entries(n):
	root.geometry("430x{}".format(400+yblock*n+80))
	remove_error_messages()
	global names
	global label
	global entry

	for name in names:
		label[name].place_forget()
		entry[name].place_forget()

	names = [str(i + 1) + ". stop" for i in range(n)]
	entry = {}
	label = {}

	label_start = Label(root, text="Start stop")
	label_start.place(x=2 * xp, y=yp + 8 * yblock + img_padding)
	valid_start = names.copy() + ["Ligegyldigt"]
	start.set("Ligegyldigt")
	drop_down_start = OptionMenu(root, start, *(valid_start))
	drop_down_start.place(x=2 * xp + 72, y=yp + 8 * yblock + img_padding)

	label_end = Label(root, text="Slut stop")
	label_end.place(x=2 * xp+ xblock, y=yp + 8 * yblock + img_padding)
	valid_end = names.copy() + ["Ligegyldigt"]
	end.set("Ligegyldigt")
	drop_down_end = OptionMenu(root, end, *(valid_end))
	drop_down_end.place(x=2 * xp + xblock + 70, y=yp + 8 * yblock + img_padding)


	for i, name in enumerate(names):
		lb = Label(root, text=name)
		lb.place(x=2 * xp, y=yp + (9 + i) * yblock + img_padding)
		label[name] = lb

		e = Entry(root)
		e.configure(fg="grey")
		e.insert(0, "Vejnavn og nummer...")
		e.place(x=2 * xp + xblock, y=yp + (9 + i) * yblock + img_padding)
		entry[name] = e
		e.bind("<FocusIn>", remove_placeholder)

	b.place_forget()
	b.place(x=2 * xp + xblock, y=yp + (9 + n + 1) * yblock + img_padding)

def adr_is_legal(addres):
	if addres == "":
		return False

	gmaps = googlemaps.Client(API_KEY)
	query = gmaps.geocode(addres)
	return len(query) != 0  # if there is a problem with the query an empty list is returned. :)

def display_error_messages(addresses, api, time, start_end):
	c = 0
	error.place(x=2 * xp, y=yp + (9 + no_of_entries.get()) * yblock + img_padding)
	if api:
		api_error.place(x=2 * xp + xblock-15, y=yp + 6 * yblock + img_padding)
		c += 1

	if time:
		time_error.place(x=2 * xp + xblock-15, y=yp + 4 * yblock+ img_padding)
		c += 1

	if start_end:
		start_error.place(x=2 * xp-15, y=yp + 8 * yblock + img_padding)
		end_error.place(x=2 * xp+ xblock-15, y=yp + 8 * yblock + img_padding)

	for no in addresses:
		n = int(no.split('.')[0])-1
		adr_errors[n].place(x=2 * xp + xblock-15, y=yp + (9 + n) * yblock + img_padding)

def remove_error_messages():
	error.place_forget()
	api_error.place_forget()
	time_error.place_forget()
	start_error.place_forget()
	end_error.place_forget()
	for er in adr_errors:
		er.place_forget()


def display_download():
	download.place(x=2 * xp, y=yp + (9 + no_of_entries.get()) * yblock + img_padding)

def remove_placeholder(event):
	event.widget.delete(0, "end")
	event.widget.configure(fg="black")
	return None

def find_route():
	remove_error_messages()
	download.place_forget()
	root.update()

	global API_KEY
	global adr_list
	error = False
	error_adr = []
	adr_list = []
	error_time = False
	error_api = False
	start_end_error = False

	API_KEY = api_code.get()
	try:
		googlemaps.Client(API_KEY)
	except ValueError:
		error = True
		error_api = True

	time = datetime(year.get(), month_dict[month.get()], day.get(), int(hour.get()), int(minute.get())).timestamp()
	if datetime.now().timestamp() > time:
		error = True
		error_time = True

	if start.get() == end.get() and start.get() != "Ligegyldigt":
		start_end_error = True
		error = True

	if not error_api:
		for name in names:
			if adr_is_legal(entry[name].get()):
				adr_list.append(entry[name].get())
			else:
				error = True
				error_adr.append(name)

	if not error:
		img1.place(x=2 * xp, y=yp + (9 + no_of_entries.get()) * yblock + img_padding)
		root.update()

		if start.get() != "Ligegyldigt":
			start_adr = entry[start.get()].get()
		else:
			start_adr = None
		if end.get() != "Ligegyldigt":
			end_adr = entry[end.get()].get()
		else:
			end_adr = None


		res = smart_bfs(adr_list, time, API_KEY, start_adr, end_adr)
		route = res[2]


		txt_writer(route, time)
		img1.place_forget()
		display_download()
	else:
		display_error_messages(error_adr, error_api, error_time, start_end_error)

def smart_bfs(locations, departure_time, API, start = None, end = None):
	gmaps = googlemaps.Client(API)

	#Change locations into a set
	locations = set(locations)

	if end != None:
		locations.remove(end)

	# initialize frontier and expanded nodes.
	expanded_nodes = dict()
	frontier = sortedlist()

	if not start == None:
		frontier.add((departure_time, (frozenset([start]), start), [start]))

	else:
		for location in locations:
			#frontier add (cost_of_route, (visited_locations, current_location), route_taken).
			frontier.add((departure_time, (frozenset([location]), location), [location]))

	# Searching the graph
	while True:
		# Choose the cheapest state (n) in the frontier and remove it from the frontier.
		n = frontier.pop(0)

		# If n is a goal state, return solution.
		if n[1][0] == locations:
			if end != None:
				try:
					cost = gmaps.directions(n[2][-1], end, mode="transit", departure_time=n[0]+1800)[0]['legs'][0]['arrival_time']['value']
				except:
					cost = gmaps.directions(n[2][-1], end, mode="transit", departure_time=n[0]+1800)[0]['legs'][0]['steps'][0]['duration']['value'] + n[0]
				visited_locations = (frozenset(n[1][0].copy().union([end])), end)
				route_taken = n[2] + [end]
				frontier.add((cost, visited_locations, route_taken))
				continue
			else:
				return n

		if end in n[1][0]:
			return n

		# We expand n
		expanded_nodes.update({n[1]:(n[0],n[2])})

		# Get outgoing routes (children) of n. i.e. those locations not yet visited in the state.
		outgoing_routes = list(locations.difference(n[1][0]))
		route_costs = get_cost_of_routes(outgoing_routes, n[1][1], n[0], API)

		for (destination,cost) in route_costs:
			visited_locations = (frozenset(n[1][0].copy().union([destination])), destination)
			route_taken = n[2] + [destination]


			# Get a list of existing states in the frontier and the expanded nodes.
			list_of_visited_locations_in_frontier = [i[1] for i in frontier]
			list_of_visited_locations_in_expanded_nodes = expanded_nodes.keys()

			# If the state has neither been expanded nor is in the frontier add it.
			if visited_locations not in list_of_visited_locations_in_frontier and visited_locations not in list_of_visited_locations_in_expanded_nodes:
				frontier.add((cost, visited_locations, route_taken))

			# Alternatively if state is already in frontier, maybe we found a better route
			elif visited_locations in list_of_visited_locations_in_frontier:

				# Get values of alternative route
				index_of_alternative_route = list_of_visited_locations_in_frontier.index(visited_locations)
				existing_cost = frontier[index_of_alternative_route][0]

				# If new route is better we update it.
				if cost < existing_cost:
					frontier.pop(index_of_alternative_route)
					frontier.add((cost, visited_locations, route_taken))



def get_cost_of_routes(destinations, origin, time, API):
	"""
	Calculates the time needed to travel from a origin destination to a list of destinations at a specific time.
	"""
	gmaps = googlemaps.Client(API)

	time_of_departure = time + 1800

	google_maps_result = gmaps.distance_matrix(origin, destinations, mode='transit', departure_time=time_of_departure)
	try:
		costs = [result['duration']['value'] + time_of_departure for result in google_maps_result['rows'][0]['elements']] # List iteration getting the cost of each route.
	except:
		google_maps_result = gmaps.distance_matrix(origin, destinations, mode='transit',
		                                           departure_time=time_of_departure)
		costs = [result['duration']['value'] + time_of_departure for result in
		         google_maps_result['rows'][0]['elements']]

	return list(zip(destinations, costs)) # Return the travel time paired up with the coresponding destination

def txt_writer(path, time):
	f = open("TDR_proposal.txt",'w+')
	gmaps = googlemaps.Client(API_KEY)

	f.write("TDR Planlæggerne har fundet følgende foreslag til en rute.\n")
	if len(path) > 8: #Hvis algorithem var heuristisk laver vi den disclaimer på det.
		f.write("Bemærk at ruten er fundet heuristisk, dvs at den måske ikke er den allermest optimale.\n")
	f.write("Husk at passe på hindanden og ikke drikke alt for meget\n\n")

	ari = str(datetime.fromtimestamp(time).time())[0:5]
	dep = str(datetime.fromtimestamp(time + 1800).time())[0:5]


	for i, (ori, des) in enumerate(zip(path, path[1:])):
		f.write("{0}. stop {1}. Ankomst kl {2}. Afgang {3}\n".format(i+1, ori, ari, dep))
		directions_result = gmaps.directions(ori,des,mode="transit",departure_time=time, language="da")

		for step in directions_result[0]['legs'][0]['steps']:
			f.write('\t' + step['html_instructions'] + '\n')
		try:
			time = directions_result[0]['legs'][0]['arrival_time']['value'] + 1800
		except:
			time += directions_result[0]['legs'][0]['steps'][0]['duration']['value'] + 1800

		ari = str(datetime.fromtimestamp(time).time())[0:5]
		dep = str(datetime.fromtimestamp(time + 1800).time())[0:5]
		f.write('\n\n')

	try:
		time = directions_result[0]['legs'][0]['arrival_time']['value'] + 1800
	except:
		time += directions_result[0]['legs'][0]['steps'][0]['duration']['value'] + 1800

	ari = str(datetime.fromtimestamp(time).time())[0:5]
	f.write("{0}. stop {1}. Ankomst kl {2}.\n".format(len(path), des, ari))
	f.close()


root = Tk()
root.title("TDR Planlæggeren")

base_folder = os.path.dirname(__file__)
logo = PhotoImage(file = os.path.join(base_folder,"img/logo3.png"))
img = Label(image = logo)
img.place(x=135,y=0)

loading = ImageTk.PhotoImage(file = os.path.join(base_folder,"img/loading.gif"))
img1 = Label(image = loading)


error = Label(root, text="Felter markeret med * indeholder fejl", fg="red")
api_error = Label(root, text="*", fg="red")
time_error = Label(root, text="*", fg="red")
adr_errors = [Label(root, text="*", fg="red") for i in range(10)]
start_error = Label(root, text="*", fg="red")
end_error = Label(root, text="*", fg="red")

b = Button(root, text="Find Rute", command=find_route)
download = Button(root, text="Se rute")
download.bind("<Button-1>", lambda e: webbrowser.open_new(
		"file://" + os.path.realpath("TDR_proposal.txt")))

api_code = Entry(root)
api_code.bind("<FocusIn>", remove_placeholder)
year = IntVar(root)
month = StringVar(root)
day = IntVar(root)
hour = StringVar(root)
minute = StringVar(root)
no_of_entries = IntVar(root)
start = StringVar(root)
end = StringVar(root)

if __name__ == '__main__':

	setup_start_window()
	mainloop()