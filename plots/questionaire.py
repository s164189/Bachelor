import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt

stops = ('1', '2', '3', '4', '5', '6', '7', '8', '9','10')
y_pos = np.arange(len(stops))
performance = [1,0,2,6,20,65,19,52,4,4]
performance2 = [[1,0,2,6,20,65,19,52,4,4]]

plt.figure(1)
plt.bar(y_pos, performance, align='center', alpha=0.5)
plt.xticks(y_pos, stops)
plt.ylabel('Hyppighed')
plt.xlabel('Antal stops')
plt.title('Fordeling af svar til rundspørge\n"Hvor mange stop havde din sidste Tour de Rus?"')

plt.show()